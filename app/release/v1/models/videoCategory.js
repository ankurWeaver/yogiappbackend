const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;
const autoId = require('../../../services/AutogenerateId');

const videoCategorySchema = new mongoose.Schema({
    _id : mongoose.Schema.Types.ObjectId,

    videoCategoryId : {
        type : String,
        default : ''
    },

    name: {
        type: String,
        default : ''
    },
      
    info : {
        type: String,
        default : ''
    },
    
    isActive : {
        type : Boolean,
        enum : [true,false],
        default : true
    },
        

    videoCreatedAt : { 
        type: Date,
        default: Date.now
    },

    // videoUrl : []
       
},
{
    timestamps: true, // add created_at , updated_at at the time of insert/update
    versionKey: false 
})

videoCategorySchema.pre('save', async function (next) {

    var videoCategory = this;
    if (this.isNew) {

        try {
            
            videoCategory._id = new mongoose.Types.ObjectId();            
            return next();

        } catch (error) {
            return next(error);
        }

    } else {
        return next();
    }
})




const videoCategory = mongoose.model("videoCategory", videoCategorySchema)

module.exports = videoCategory