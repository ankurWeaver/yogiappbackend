const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const SALT_WORK_FACTOR = 10;
const autoId = require('../../../services/AutogenerateId');

const userSchema = new mongoose.Schema({
    _id : mongoose.Schema.Types.ObjectId,
    userId : {
        type : String
    },
    name: {
        type: String
    },
    
    
    email : { 
        type: String, 
        lowercase: true, 
        trim: true,
        match : [/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/,'Please fill valid email address'],
        validate: {
            validator: function() {
              return new Promise((res, rej) =>{
                User.findOne({email: this.email,_id: {$ne: this._id}})
                    .then(data => {
                        if(data) {
                            res(false)
                        } else {
                            res(true)
                        }
                    })
                    .catch(err => {
                        res(false)
                    })
              })
            }, message: 'Email Already Taken'
          } 
    },
    mobile : { 
        type: String,
        get : v => parseInt(v),
        trim: true,
        
        match : [/^[0-9]*$/,'Please fill valid mobile number'],
        
    },
    password : {
        type: String,
        required: true
    },
    
    isActive : {
        type : Boolean,
        enum : [true,false],
        default : true
    },
        

    isLoggedIn : {
        type : Boolean,
        enum : [true,false],
        default : false
    },
    profileCreatedAt : { 
        type: Date,
        default: Date.now
    },
    

   
},
{
    timestamps: true, // add created_at , updated_at at the time of insert/update
    versionKey: false 
})

userSchema.pre('save', async function (next) {

    var user = this;
    if (this.isModified('password') || this.isNew) {

        try {
            var salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
            var hashed_password = await bcrypt.hash(user.password, salt)
            user.password = hashed_password;
            user._id = new mongoose.Types.ObjectId();
            
            
            return next();
        } catch (error) {
            return next(error);
        }

    } else {
        return next();
    }
})

userSchema.methods.comparePassword = function (pw, cb) {
    bcrypt.compare(pw, this.password, function (err, isMatched) {
        if (err) return cb({"err from here":err})
        cb(null, isMatched)
    })
}


const User = mongoose.model("User", userSchema)

module.exports = User