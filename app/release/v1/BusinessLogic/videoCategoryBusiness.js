const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const User = require("../models/user");

const bcrypt = require("bcrypt");
const SALT_WORK_FACTOR = 10;
const Mailercontroller = require("../../../services/mailer");
const errorMsgJSON = require("../../../services/errors.json");
const config = require("../config/globals/keyfile");
const AutogenerateIdcontroller = require('../../../services/AutogenerateId');


//===========================================


// this function is required to check Whether the guest exist or not
exports.checkGuestExistance = async (email) => {	
	return new Promise(function(resolve, reject){
        
        let aggrQuery = [
            {
	            '$match': { 
	              'email': email,
	            }  
            },   
        ];

        Guest.aggregate(aggrQuery, function (err, response) { // start of fetch function
        	if (err) {  // When Error Occur
	            resolve({   
	                "isError": true,
	            });
	        } else {
	        	if (response.length < 1) {
                    resolve({   
		                "isError": false,
		                "isGuestExist": false
		            }); 
	        	} else {
	        		resolve({   
		                "isError": false,
		                "isGuestExist": true
		            }); 
	        	}      
	        }
        }) // END User.aggregate(aggrQuery, function (fetcherr, fetchresponse) {		
    }) // END Promise	
}

