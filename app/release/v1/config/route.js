const express = require('express');
const router = express.Router();

const mongoose = require('mongoose');



// localhost connection 
// mongoose.connect('mongodb+srv://miouser:1q1q1q1@mio-tgaxo.mongodb.net/mio?retryWrites=true&w=majority', {
mongoose.connect('mongodb://localhost:27017/yogiAppDb', {
  useNewUrlParser: true,
  useUnifiedTopology: true
})
mongoose.connection.on('connected', function () {
  console.log(' Mongoose default connection open to ' + "mongodb://localhost:27017/yogiAppDb");
});

// If the connection throws an error
mongoose.connection.on('error', function (err) {
  console.log('Mongoose default connection error: ' + err);
});
// When the connection is disconnected
mongoose.connection.on('disconnected', function () {
  console.log('Mongoose default connection disconnected');
});

// If the Node process ends, close the Mongoose connection 
process.on('SIGINT', function () {
  mongoose.connection.close(function () {
    console.log('Mongoose default connection disconnected through app termination');
    process.exit(0);
  });
});



//aws connection
// mongoose.connect('mongodb://username:password@ip_address:port/db_name',{
//     useNewUrlParser:true
// })


const userRoute = require('./endpoints/user');
const videoCategoryRoute = require('./endpoints/videoCategory');


/**
 *  ROUTES -  ROOT LEVEL
 */
router.use("/user", userRoute)
router.use("/video", videoCategoryRoute)



module.exports = router
