const express = require('express');
const router = express.Router();
const userCtrl = require('../../controllers/users');
const checkAuth = require('../../middleware/check-auth');
router.use(express.json());
router.use(express.urlencoded({extended:true}))



router.post('/sign_up', userCtrl.signUp);
router.post('/sign_in', userCtrl.signIn);

// router.post('/join-conference-with-code', userCtrl.joinConferenceWithCode);



module.exports = router;