const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const User = require("../models/user");
const videoCategory = require("../models/videoCategory");

const bcrypt = require("bcrypt");
const SALT_WORK_FACTOR = 10;
const Mailercontroller = require("../../../services/mailer");
const errorMsgJSON = require("../../../services/errors.json");
const config = require("../config/globals/keyfile");
const AutogenerateIdcontroller = require('../../../services/AutogenerateId');

const UserBusiness = require("../BusinessLogic/userBusiness");
const VideoCategoryBusiness = require("../BusinessLogic/videoCategoryBusiness");





module.exports = {
  /** ******************************* VIDIO-RELATED FEATURES start__******************************** */

  // This function is used to add video category detail 
  addVideo: (req, res, next)=> {

        let lang = req.headers.language ? req.headers.language : "EN";
        try {

            let name = req.body.name ? req.body.name: res.json({
              isError: true,
              message: errorMsgJSON[lang]["303"] + " -Name of Video",
            }); 

            let info = req.body.info ? req.body.info: res.json({
              isError: true,
              message: errorMsgJSON[lang]["303"] + " -Information of Video",
            }); 

            let duration = req.body.duration ? req.body.duration: res.json({
              isError: true,
              message: errorMsgJSON[lang]["303"] + " -duration of Video",
            }); 

            // Here, Circular json avoided
            if (!req.body.name || !req.body.info || !req.body.duration) { return; }  

                  let newVideoCategory = new videoCategory({
                  
                      name: name,
                      info: info,
                      duration: duration
                     
                  });

                  newVideoCategory.save(function(err, item) {
                        if (item) {
                            res.json({
                              isError: false,
                              message: errorMsgJSON[lang]["200"],
                              statuscode: 200,
                              details: item
                            });

                            return;                                  
                        }


                        if (err) {
                          
                            res.json({
                              isError: true,
                              message: errorMsgJSON[lang]["404"],
                              statuscode: 404,
                              details: err["message"]     
                            });

                            return;
                        }
                  });  // END newVideoCategory.save(function(err, item) {
         
    } catch(error) {
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["404"],
          message: "Error",
          statuscode: 404,
          details: null
        })
    }

  },

  
  
  /** ******************************* VIDIO-RELATED FEATURES __end ******************************** */
};
