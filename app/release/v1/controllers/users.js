const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const User = require("../models/user");

const bcrypt = require("bcrypt");
const SALT_WORK_FACTOR = 10;
const Mailercontroller = require("../../../services/mailer");
const errorMsgJSON = require("../../../services/errors.json");
const config = require("../config/globals/keyfile");
const AutogenerateIdcontroller = require('../../../services/AutogenerateId');

const UserBusiness = require("../BusinessLogic/userBusiness");





module.exports = {
  /** ******************************* AUTH-RELATED FEATURES start__******************************** */

  // this function is written for sign up function
  signUp: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    var name = req.body.name
      ? req.body.name
      : res.json({
          isError: true,
          statuscode: 303,
          details: null,
          message: errorMsgJSON[lang]["303"] + " first name"
        });
     
    var email = req.body.email
      ? req.body.email
      : res.json({
          isError: true,
          statuscode: 303,
          details: null,
          message: errorMsgJSON[lang]["303"] + " email"
        });
    var mobile = req.body.mobile
      ? req.body.mobile
      : res.json({
          isError: true,
          statuscode: 303,
          details: null,
          message: errorMsgJSON[lang]["303"] + " mobile"
        });
    var password = req.body.password
      ? req.body.password
      : res.json({
          isError: true,
          statuscode: 303,
          details: null,
          message: errorMsgJSON[lang]["303"] + " password"
        });

    // Here, Circular json avoided
    if (!req.body.name || !req.body.email || !req.body.mobile || !req.body.password) { return; }  
    
    let usrTypeBlock = 'YUSR'
    let userId = AutogenerateIdcontroller.autogenerateId(usrTypeBlock);     

    try {      
                  let newUser = new User({
                      userId: userId,
                      name: name,
                      email: email,
                      mobile: mobile,
                      password: password
                  });

                  newUser.save(function(err, item) {
                        if (item) {

                            res.json({
                              isError: false,
                              message: errorMsgJSON[lang]["200"],
                              statuscode: 200,
                              details: item
                            }); 
                                 
                        }
                        if (err) {
                          console.log(err);
                          res.json({
                            isError: true,
                            message: errorMsgJSON[lang]["404"],
                            statuscode: 404,
                            details: err["message"]     
                          });
                        }
                  });  // END newUser.save(function(err, item) {
      

    } catch (e) {
      res.json({
        isError: true,
        message: errorMsgJSON[lang]["404"],
        statuscode: 400,
        details: e
      });
    }
  },

  /**
   * @description :  sign in process using email and password
   */
  signIn: (req, res, next) => {

    let lang = req.headers.language ? req.headers.language : "EN";
    var email = req.body.email
      ? req.body.email
      : res.json({
          isError: true,
          statuscode: 303,
          details: null,
          message: errorMsgJSON[lang]["303"] + "Please provide email"
        });
    var password = req.body.password
      ? req.body.password
      : res.json({
          isError: true,
          statuscode: 303,
          details: null,
          message: errorMsgJSON[lang]["303"] + "Please provide password"
        });

    // Here, Circular json avoided
    if (!req.body.email || !req.body.password) { return; }  

    let findUserQry = {
      email: email
    };
    
    User.findOne(findUserQry, function(err, item) {

      if (err) {
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["400"],
          statuscode: 400,
          details: null
        });
      } else {
        if (!item) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["400"],
            statuscode: 400,
            details: null
          });
        } else {
          item.comparePassword(password, async function(err, isMatch) {
            if (isMatch && !err) {
              try {
                user = JSON.parse(JSON.stringify(item));
                delete user.password;
                var token = await jwt.sign(user, config.secret.d, {
                  expiresIn: "12h"
                });
                //Update user isLoggedIn status to true

                User.updateOne(
                  findUserQry,
                  {
                    isLoggedIn: true
                  },
                  function(err, item) {
                    if (err) {
                      res.json({
                        isError: true,
                        message: errorMsgJSON[lang]["400"],
                        statuscode: 400,
                        details: null
                      });
                    } else {

                      //CompanyDetail.findOne({_id: user.companyId}, function(err, companyDetail) {

                          // if (err) {
                          //     res.json({
                          //         isError: false,
                          //         message: errorMsgJSON[lang]["200"],
                          //         statuscode: 200,
                          //         details: {
                          //           token: "JWT " + token,
                          //           data: user
                          //         }
                          //     });

                          //     return;

                          // }
                          

                          res.json({
                            isError: false,
                            message: errorMsgJSON[lang]["200"],
                            statuscode: 200,
                            details: {
                              token: "JWT " + token,
                              data: user
                            }
                          });

                      // })          
                    }
                  }
                );
              } catch (error) {
                return res.json({
                  isError: true,
                  message: "Auth failed." + JSON.stringify(error),
                  statuscode: 400,
                  data: null
                });
              }
            } else {
              return res.json({
                isError: true,
                message: "Auth failed.",
                statuscode: 400,
                data: []
              });
            }
          });
        }
      }
    });
    

    //==========
  },

  /**
   * @description :   forget password feature, will send email to registered email with OTP
   */
  forgotPassword: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    let email = req.body.email
      ? req.body.email
      : res.json({
          isError: true,
          statuscode: 303,
          details: null,
          message: errorMsgJSON[lang]["303"] + " - email "
        });
    let query = { email: email };

    User.find(query, function(err, item) {

      if (err) {
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["400"],
          statuscode: 400,
          details: null
        });
      } else {



        if (item.length == 0) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["204"],
            statuscode: 204,
            details: null
          });
        } else {
          let randomNumber =
            Math.floor(Math.random() * (999999 - 111111 + 1)) + 111111;
          var otpMailDetails = {
            receiver: email,
            subject: "OTP",
            message: "Hi, Your OTP for the verification is " + randomNumber
          };

          Mailercontroller.viaGmail(otpMailDetails, (err, data) => {
            
            if (err) {
              // res.json({
              //   isError: true,
              //   message: errorMsgJSON[lang]["401"],
              //   //otp: 401,
              //   statuscode: 401,
              //   details: null
              // });

              //================================
              //has to be removed
              var updatevalueswith = {
                $set: {
                  otp: randomNumber
                }
              };
              User.updateOne(query, updatevalueswith, function(err, res1) {
                if (err) {
                  res.json({
                    isError: true,
                    message: errorMsgJSON[lang]["400"],
                    statuscode: 400,
                    details: null
                  });
                }
                if (res1.nModified > 0) {
                  res.json({
                    isError: false,
                    message: errorMsgJSON[lang]["203"],
                    otp: randomNumber,
                    statuscode: 200,
                    details: data
                  });
                }
              });
              //======================
            } else {
              var updatevalueswith = {
                $set: {
                  otp: randomNumber
                }
              };
              User.updateOne(query, updatevalueswith, function(err, res1) {
                if (err) {
                  res.json({
                    isError: true,
                    message: errorMsgJSON[lang]["400"],
                    statuscode: 400,
                    details: null
                  });
                }
                if (res1.nModified > 0) {
                  res.json({
                    isError: false,
                    message: errorMsgJSON[lang]["203"],
                    statuscode: 200,
                    details: data
                  });
                }
              });
            }
          });
        }
      }
    });
  },

  /**
   * @description :   reset password feature, if OTP matches for the user then update password
   */
  resetPassword: async (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    let email = req.body.email
      ? req.body.email
      : res.json({
          isError: true,
          statuscode: 303,
          details: null,
          message: errorMsgJSON[lang]["303"] + " - Email "
        });
    let otp = req.body.otp
      ? req.body.otp
      : res.json({
          isError: true,
          statuscode: 303,
          details: null,
          message: errorMsgJSON[lang]["303"] + " - otp "
        });
    let newPassword = req.body.new_password
      ? req.body.new_password
      : res.json({
          isError: true,
          statuscode: 303,
          details: null,
          message: errorMsgJSON[lang]["303"] + " - New Password "
        });

    var salt = await bcrypt.genSalt(SALT_WORK_FACTOR);
    let hashedNewpassword = await bcrypt.hash(newPassword, salt);

    let query = { email: email };

    User.find(query, function(err, item) {
      if (err) {
        res.json({
          isError: true,
          message: errorMsgJSON[lang]["400"],
          statuscode: 400,
          details: null
        });
      } else {
        if (item.length == 0) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["503"],
            statuscode: 503,
            details: null
          });
        } else {
          if (item[0].otp.toString().trim() === otp.toString().trim()) {
            var updatevalueswith = {
              $set: {
                password: hashedNewpassword
              }
            };
            User.updateOne(query, updatevalueswith, function(err, res1) {
              if (err) {
                res.json({
                  isError: true,
                  message: errorMsgJSON[lang]["400"],
                  statuscode: 400,
                  details: null
                });
              }
              if (res1.nModified > 0) {
                res.json({
                  isError: false,
                  message:
                    errorMsgJSON[lang]["200"] +
                    " Password Changed Successfully",
                  statuscode: 200,
                  details: null
                });
              }
            });
          } else {
            res.json({
              isError: true,
              message: errorMsgJSON[lang]["503"],
              statuscode: 503,
              details: null
            });
          }
        }
      }
    });
  },

  /**
   * @description : sign out functionality
   */

  signOut: (req, res, next) => {
    let lang = req.headers.language ? req.headers.language : "EN";
    var email = req.body.email
      ? req.body.email
      : res.json({
          isError: true,
          statuscode: 303,
          details: null,
          message: errorMsgJSON[lang]["303"] + "Please provide email"
        });

    let findUserQry = {
      email: email
    };

    User.updateOne(
      findUserQry,
      {
        isLoggedIn: false
      },
      function(err, item) {
        if (err) {
          res.json({
            isError: true,
            message: errorMsgJSON[lang]["400"],
            statuscode: 400,
            details: null
          });
        } else {
          res.json({
            isError: false,
            message: errorMsgJSON[lang]["200"],
            statuscode: 200,
            details: null
          });
        }
      }
    );
  }

  /** ******************************* AUTH-RELATED FEATURES __end ******************************** */
};
