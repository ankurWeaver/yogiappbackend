var winston = require('winston');

var logger = winston.createLogger({
  transports: [
    new winston.transports.Console(),
    new winston.transports.File({ filename: './all-logs.log' }),
  ],
  exceptionHandlers: [
    new winston.transports.File({ filename: './exceptions.log' })
  ]
});

module.exports = logger;